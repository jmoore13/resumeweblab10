var mysql   = require('mysql');
var db  = require('./db_connection.js');

var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM company;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(company_id, callback) {
    var query = 'SELECT company_name FROM company ' +
        'WHERE company_id = ?';
    var queryData = [company_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO company (company_name) VALUES (?)';

    var queryData = [params.company_name];
    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });


};

exports.delete = function(company_id, callback) {
    var query = 'DELETE FROM company WHERE company_id = ?';
    var queryData = [company_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.updateCompany= function(params, callback) {
    var query = 'UPDATE company SET company_id = ? WHERE company_id = ?';
    var queryData = [params.company_name, params.company_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.edit = function(company_id, company_name, callback) {
    var query = 'CALL company_getinfo(?)';
    var queryData = [company_id, company_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};