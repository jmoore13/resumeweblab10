var mysql   = require('mysql');
var db  = require('./db_connection.js');

var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(account_id, callback) {
    var query = 'SELECT first_name, last_name, email FROM account ' +
        'WHERE account.account_id = ?';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO account (first_name, last_name, email) VALUES (?, ?, ?)';

    var queryData = [params.first_name, params.last_name, params.email];
    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.delete = function(account_id, callback) {
    var query = 'DELETE FROM account WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.updateAccount = function(params, callback) {
    var query = 'UPDATE account SET first_name = ? last_name = ? email = ? WHERE account_id = ?';
    var queryData = [params.first_name, params.last_name, params.email, params.account_id];

    connection.query(query, queryData, function(err, result) {
                callback(err, result);
    });
};

exports.edit = function(account_id, first_name, last_name, email, callback) {
    var query = 'CALL account_getinfo(?)';
    var queryData = [account_id, first_name, last_name, email];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};